import './App.css';

declare
var ZoomMtg

ZoomMtg.setZoomJSLib('https://source.zoom.us/1.9.6/lib', '/av');

ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();
// loads language files, also passes any error messages to the ui
ZoomMtg.i18n.load('en-US');
ZoomMtg.i18n.reload('en-US');

function App() {

    // setup your signature endpoint here: https://github.com/zoom/websdk-sample-signature-node.js
    let signatureEndpoint = 'https://websdk-sample.herokuapp.com/'
    let apiKey = 'CXbqG9lzT-KTW4wkHtYYcA'
    let meetingNumber = '72829898498'
    let role = 0
    let leaveUrl = 'http://localhost:3000'
    let userName = 'React'
    let userEmail = ''
    let passWord = 'a2FYzv'

    function getSignature(e) {
        e.preventDefault();

        fetch(signatureEndpoint, {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({
                    meetingNumber: meetingNumber,
                    role: role
                })
            }).then(res => res.json())
            .then(response => {
                startMeeting(response.signature)
            }).catch(error => {
                console.error(error)
            })
    }

    function startMeeting(signature) {
        document.getElementById('zmmtg-root').style.display = 'block'

        ZoomMtg.init({
            leaveUrl: leaveUrl,
            isSupportAV: true,
            success: (success) => {
                console.log(success)

                ZoomMtg.join({
                    signature: signature,
                    meetingNumber: meetingNumber,
                    userName: userName,
                    apiKey: apiKey,
                    userEmail: userEmail,
                    passWord: passWord,
                    success: (success) => {
                        console.log(success)
                    },
                    error: (error) => {
                        console.log(error)
                    }
                })

            },
            error: (error) => {
                console.log(error)
            }
        })
    }

    return ( 
    //     <div className="App">
    //         <div class="iframe-container" class="tes">
    // 	<iframe allow="microphone; camera" class="tes2" src={`https://zoom.us/wc/join/${meetingNumber}`} sandbox="allow-forms allow-scripts" frameborder="0"></iframe>
	// </div>
            <main>
                <h1> Zoom WebSDK Sample React </h1>
                <button onClick={getSignature}> Join Meeting </button>
            </main>
        // </div>
    );
}

export default App;